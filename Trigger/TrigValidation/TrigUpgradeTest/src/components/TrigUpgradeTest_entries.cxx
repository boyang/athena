#include "../ITestHypoTool.h"
#include "../TestHypoAlg.h"
#include "../TestHypoTool.h"
#include "../TestMHypoTool.h"
#include "../TestRecoAlg.h"
#include "../TestRoRSeqFilter.h"
#include "../TestMerger.h"
#include "../TestComboHypoAlg.h"
#include "../TestInputMaker.h"


DECLARE_COMPONENT( HLTTest::TestInputMaker )
DECLARE_COMPONENT( HLTTest::TestHypoAlg )
DECLARE_COMPONENT( HLTTest::TestHypoTool )
DECLARE_COMPONENT( HLTTest::TestMHypoTool )
DECLARE_COMPONENT( HLTTest::TestRecoAlg )
DECLARE_COMPONENT( HLTTest::TestRoRSeqFilter )
DECLARE_COMPONENT( HLTTest::TestMerger )
DECLARE_COMPONENT( HLTTest::TestComboHypoAlg )

